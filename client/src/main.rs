use std::io;
use std::io::{Write, Read};
use std::net::TcpStream;

fn main() {
    let addr = "polaris.kokarde.fr:8787";
    let mut serveur = TcpStream::connect(addr).unwrap();
    println!("Quel est l'age du capitaine ?");
    loop {
        let mut buffer = String::new();
        println!("Je dirais ...");
        io::stdin().read_line(&mut buffer).unwrap();
        let trimmed = buffer.trim();
        let guess = trimmed.parse::<u8>().unwrap();
        serveur.write(&[guess]).unwrap();
        serveur.flush().unwrap();
        let mut buffer = [0; 1];
        serveur.read(&mut buffer).unwrap();
        if buffer[0] == 2{
            println!("Bien joué, c'est le bon age");
            return
        }
        else if buffer[0] == 0{
            println!("Non, la capitaine est plus vieux que ca");
        }
        else if buffer[0] == 1{
            println!("Non, le capitaine est plus jeune que ca");
        }
    }
}
