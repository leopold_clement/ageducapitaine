use std::io::*;
use std::net::{TcpListener, TcpStream};
use std::thread;

fn main() {
    let listener = TcpListener::bind("0.0.0.0:8787").unwrap();
    for stream in listener.incoming() {
        let stream = stream.unwrap();
        thread::spawn(|| {
            handle_connection(stream);
        });
    }
}

fn handle_connection(mut stream: TcpStream) {
    let age = rand::random::<u8>();
    let mut guess: u8;
    let mut buffer = [0; 1];
    loop {
        stream.read(&mut buffer).unwrap();
        guess = buffer[0];
        let mut reponse = [0; 1];
        if guess == age {
            reponse[0] = 2;
        } else if guess < age {
            reponse[0] = 0;
        } else if guess > age {
            reponse[0] = 1;
        }
        stream.write(&reponse).unwrap();
        stream.flush().unwrap();
        if reponse[0] == 2 {
            return;
        }
    }
}
